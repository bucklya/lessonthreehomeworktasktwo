package com.company;

import javax.sound.midi.Soundbank;
import java.util.Scanner;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        System.out.println("Программа предназначенна для вывода строк длина которых меньше длины средней.");
        System.out.println("Введите сколько строк вы хотите проверить: ");
        Scanner numberOfStringsIn = new Scanner(System.in);
        int numberOfStrings = Integer.parseInt(numberOfStringsIn.nextLine());
        System.out.println("Введите " + numberOfStrings + " строк(и)");
        String[] arrayOfStrings = new String[numberOfStrings];

        for (int i = 0; i < numberOfStrings; i++) {
            Scanner stringIn = new Scanner(System.in);
            arrayOfStrings[i] = stringIn.nextLine();
        }

        for (int i = arrayOfStrings.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arrayOfStrings[j].length() > arrayOfStrings[j + 1].length()) {
                    String helper = arrayOfStrings[j];
                    arrayOfStrings[j] = arrayOfStrings[j + 1];
                    arrayOfStrings[j + 1] = helper;
                }
            }
        }

        numberOfStrings = numberOfStrings / 2;
        for (int i = 0; i <= numberOfStrings - 1; i++) {
            System.out.println(arrayOfStrings[i]);
        }

    }
}

